<?php
/**
 * EstadoConsulta Active Record
 * @author  <your-name-here>
 */
class EstadoConsulta extends TRecord
{
    const TABLENAME = 'estado_consulta';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'serial'; // {max, serial}
    
    
    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('descricao');
    }

    
    /**
     * Method getConsultas
     */
    public function getConsultas()
    {
        $criteria = new TCriteria;
        $criteria->add(new TFilter('estadoconsulta_id', '=', $this->id));
        return Consulta::getObjects( $criteria );
    }
    


}

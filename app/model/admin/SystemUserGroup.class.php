<?php
/**
 * System_user_group Active Record
 * @author  <your-name-here>
 */
class SystemUserGroup extends TRecord
{
    const TABLENAME = 'system_user_group';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial}
    
    
    /**
     * Constructor method
     */
    public function __construct($id = NULL)
    {
        parent::__construct($id);
        parent::addAttribute('system_user_id');
        parent::addAttribute('system_group_id');
    }
    
    public static function isMedico ($id)
    {
        TTransaction::open('permission');
        
            $repos = new TRepository('SystemUserGroup');
            $repos = $repos->load();
            foreach ($repos as $r )
            {
                if($r->system_user_id == $id and $r->system_group_id == 3)
                {
                    TTransaction::close();
                    return true;
                }
            }
        TTransaction::close();
        return false;
    }
}
?>
<?php
/**
 * Consultorio Active Record
 * @author  <your-name-here>
 */
class Consultorio extends TRecord
{
    const TABLENAME = 'consultorio';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'serial'; // {max, serial}
    


    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('id');
        parent::addAttribute('sala');
        parent::addAttribute('predio');
    }
    public function get_consultorio_nome()
    {
        $consultorio = new Consultorio($this->id);        
        $nome = $consultorio->sala . " - " . $consultorio.predio;
        return $nome;
    }
}

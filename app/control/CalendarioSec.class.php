<?php
class CalendarioSec extends TPage 
{    
    private $html; 
    private $datas;   
    
    public function __construct( $param )
    {
        parent::__construct();
        
        $this->html = new THtmlRenderer('app/resources/calendarioSec.html');                    
    
        $this->montarDadosCalendarioSec( date('m') , date('Y') );       
        
        $this->html->enableSection( 'main');
        $this->html->enableSection( 'data' , $this->datas , true );      
        
        parent::add($this->html);
        
    }
    
    public function getConsultasSec( $mes , $ano )
    {
        TTransaction::open('db_consultas');
            
        $dataMax = date("Y-m-t", mktime(0,0,0,$mes,'01',$ano));      
        
        $dataMin = $ano.'-'.$mes.'-'.'01';
                
        $consultas = new TRepository( 'Consulta' );
        
        $criterio = new TCriteria();        
        $criterio->add( new TFilter( 'estado_consulta_id', '!=' , 2 ) );

        $consultas = $consultas->load( $criterio );       
                
        TTransaction::close();  
             
        return $consultas;
    }
    
    public function montarDadosCalendarioSec( $mes , $ano )
    {
        $consultas = $this->getConsultasSec( $mes, $ano );       
        
        $this->consultasCalendarioSec = array();
        
        try
        {
            $dataMax = date("t", mktime(0,0,0,$mes,'01',$ano));

            TTransaction::open('db_consultas');
            for( $i=0; $i < 10 ; $i++ )
            {
                $consultasDia[$i] = array();
                
                $consultasManha = array();
                $consultasTarde = array();
                
                foreach( $consultas as $consulta )
                {
                    if ($consulta->dt_consulta == date('Y-m-d', strtotime("+$i days") ) )
                    {
                        $paciente = new Paciente( $consulta->paciente_id );               
                        $estado = new EstadoConsulta($consulta->estado_consulta_id);
                        TTransaction::open('permission');
                        $medico = new SystemUser($consulta->system_user_id);  
                        TTransaction::close();                       
                        if( $consulta->turno == 'M' )
                        {
                            $consultasManha[]= array('paciente' => $paciente->nome , 'id' => $consulta->id , 'estado' => $estado->descricao , 'medico' => $medico->name);                    
                        }
                        else
                        {
                            $consultasTarde[] = array ('paciente' => $paciente->nome , 'id' => $consulta->id , 'estado' => $estado->descricao , 'medico' => $medico->name);
                        }
                    }                   
                }
                
                
                //adiciona data ao vetor de datas               

                $cM = new THtmlRenderer('app/resources/consultas.html');
                $cM->enableSection('main', $consultasManha, TRUE);
                $cT = new THtmlRenderer('app/resources/consultas.html');
                $cT->enableSection('main', $consultasTarde, TRUE);
                
                $datas['consultaManha'] = $cM;
                $datas['consultaTarde'] = $cT;
                $datas['data'] = date('Y-m-d', strtotime("+$i days") );
                $datas['cor']  = ($datas['data'] <= date("d-m-Y") ) ? "white" : "white" ;  

                
                $this->datas[] = $datas;
                
                
                 
            }
                                    
            TTransaction::close();
            //var_dump($this->consultasCalendarioSec);
        }
        catch ( Exception $e )
        {
            new TMessage ( 'error' , $e->getMessage() );
            
            TTransaction::rollback();
        }
    }
}

?>
<?php
/**
 * AvisosForm Registration
 * @author  <your name here>
 */
class AvisosForm extends TStandardForm
{
    protected $form; // form
    
    /**
     * Class constructor
     * Creates the page and the registration form
     */
    function __construct()
    {
        parent::__construct();
        
        parent::setDatabase('db_consultas');              // defines the database
        parent::setActiveRecord('Avisos');     // defines the active record
        
        // creates the form
        $this->form = new TQuickForm('form_Avisos');
        $this->form->class = 'tform'; // CSS class
        $this->form->style = 'width: 100%';
        
        // define the form title
        $this->form->setFormTitle('Avisos');
        


        // create the form fields
        $id                             = new TEntry('id');
        $nome                           = new TEntry('nome');
        $mensagem                       = new TText('mensagem');
        $dt_aviso                       = new TEntry('dt_aviso');
        $estado                         = new THidden('estado');


        // add the fields
        $this->form->addQuickField('ID', $id,  500);
        $this->form->addQuickField('Data Aviso', $dt_aviso,  500);
        $this->form->addQuickField('Nome', $nome,  500);
        $this->form->addQuickField('Mensagem', $mensagem,  500, new TRequiredValidator);        
        $this->form->addQuickField('estado', $estado,  500);


        $mensagem->setSize(500, 100);
        
        // setEditable
        $id->setEditable( FALSE );    
        $dt_aviso->setEditable( FALSE );    
        $nome->setEditable( FALSE );
        $estado->setValue('A');
        
        // setValues
        $nome->setValue(TSession::getValue('username'));
        $dt_aviso->setValue( date( 'Y-m-d ') );
        
        // create the form actions
        $this->form->addQuickAction(_t('Save'), new TAction(array($this, 'onSave')), 'ico_save.png');
        $this->form->addQuickAction(_t('New'),  new TAction(array($this, 'onEdit')), 'ico_new.png');
        
        // add the form to the page
        parent::add($this->form);
    }
    
    public function onEdit($param)
    {
        try
        {
            if (isset($param['key']))
            {
                // get the parameter $key
                $key=$param['key'];
                
                // open a transaction with database
                TTransaction::open($this->database);
                
                $class = $this->activeRecord;
                
                // instantiates object
                $object = new $class($key);
                
                // fill the form with the active record data
                $this->form->setData($object);
                
                // close the transaction
                TTransaction::close();
            }
            else
            {
                $this->form->clear();
                $obj->nome = TSession::getValue('username');
                $obj->dt_aviso = date('Y-m-d');
                $obj->estado = 'A';
                
                $this->form->setData($obj);
            }
        }
        catch (Exception $e) // in case of exception
        {
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }
}

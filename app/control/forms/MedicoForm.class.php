<?php
/**
 * System_userForm Registration
 * @author  <your name here>
 */
class MedicoForm extends TPage
{
    protected $form; // form
    
    /**
     * Class constructor
     * Creates the page and the registration form
     */
    function __construct()
    {
        parent::__construct();
        // creates the form
        $this->form = new TForm('form_System_user');
        $this->form->class = 'tform';

        // creates the table container
        $table = new TTable;
        $table->style = 'width: 100%';
        
        $table->addRowSet( new TLabel(('Médicos')), '', '','' )->class = 'tformtitle';
        
        // add the table inside the form
        $this->form->add($table);


        // create the form fields
        $id                  = new TEntry('id');
        $name                = new TEntry('name');
        $login               = new TEntry('login');
        $password            = new TPassword('password');
        $repassword          = new TPassword('repassword');
        $email               = new TEntry('email');        
        $frontpage_id        = new THidden('frontpage_id');
        $groups              = new THidden('groups');
        
        
        // define the sizes
        $id->setSize(200);
        $name->setSize(200);
        $login->setSize(200);
        $password->setSize(200);
        $email->setSize(200);
        
        // editable
        $id->setEditable(false);
        
        
        //default values
        $frontpage_id->setValue( 26 );
        $groups->setValue( '3' );
        
        // validations
        $name->addValidation(_t('Name'), new TRequiredValidator);
        $login->addValidation('Login', new TRequiredValidator);
        $email->addValidation('Email', new TEmailValidator);


        
        // add a row for the field id
        $table->addRowSet(new TLabel('ID'),      $id,           new TLabel(('Nome') ), $name);
        $table->addRowSet(new TLabel(('Login')), $login,        new TLabel(('E-mail')), $email);
        $table->addRowSet(new TLabel(('Senha')), $password,     new TLabel(('Confirmar Senha')), $repassword);
        $table->addRowSet(new TLabel(('')),      $frontpage_id, new TLabel(('')) , $groups);
        

        // create an action button (save)
        $save_button=new TButton('save');
        $save_button->setAction(new TAction(array($this, 'onSave')), _t('Save'));
        $save_button->setImage('ico_save.png');
        
        // create an new button (edit with no parameters)
        $new_button=new TButton('new');
        $new_button->setAction(new TAction(array($this, 'onEdit')), _t('New'));
        $new_button->setImage('ico_new.png');
        
        
        // define the form fields
        $this->form->setFields(array($id,$name,$login,$password,$repassword,$frontpage_id, $groups,$email,$save_button,$new_button));
        
        $buttons = new THBox;
        $buttons->add($save_button);
        $buttons->add($new_button);

        $row=$table->addRow();
        $row->class = 'tformaction';
        $cell = $row->addCell( $buttons );
        $cell->colspan = 4;

        $container = new TTable;
        $container->style = 'width: 100%';
        $container->addRow()->addCell($this->form);

        // add the form to the page
        parent::add($container);
    }

    /**
     * method onSave()
     * Executed whenever the user clicks at the save button
     */
    function onSave( $param )
    {
            try
            {
                // open a transaction with database 'permission'
                TTransaction::open('permission');
                
                // get the form data into an active record System_user
                $object = $this->form->getData('SystemUser');
    
                // form validation
                $this->form->validate();
                
                $senha = $object->password;
                
                if( ! $object->id )
                {
                    if( ! $object->password )
                        throw new Exception(TAdiantiCoreTranslator::translate('The field ^1 is required', _t('Password')));
                }
                
                if( $object->password )
                {
                    if( $object->password != $object->repassword )
                        throw new Exception(_t('The passwords do not match'));
                    
                    $object->password = md5($object->password);
                }
                else
                    unset($object->password);            
                
                //Gravando como médico
                $object->addSystemUserGroup( new SystemGroup($object->groups) );        
                
                $object->store(); // stores the object
                
                $object->password = $senha;
                
                // fill the form with the active record data
                $this->form->setData($object);
                
                // close the transaction
                TTransaction::close();
                
                // shows the success message
                new TMessage('info', TAdiantiCoreTranslator::translate('Record saved'));
                // reload the listing
            }
            catch (Exception $e) // in case of exception
            {
                // shows the exception error message
                new TMessage('error', '<b>Error</b> ' . $e->getMessage());
                
                // undo all pending operations
                TTransaction::rollback();
            }           
    }
    
    /**
     * method onEdit()
     * Executed whenever the user clicks at the edit button da datagrid
     */
    function onEdit($param)
    {
        try
        {
            if (isset($param['key']))
            {
                // get the parameter $key
                $key=$param['key'];
                
                // open a transaction with database 'permission'
                TTransaction::open('permission');
                
                // instantiates object System_user
                
                $object = new SystemUser($key);
                
                unset($object->password);               

                
                // fill the form with the active record data
                $this->form->setData($object);
                
                // close the transaction
                TTransaction::close();
            }
            else
            {
                $this->form->clear();
            }
        }
        catch (Exception $e) // in case of exception
        {
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            
            // undo all pending operations
            TTransaction::rollback();
        }
    }
}
?>
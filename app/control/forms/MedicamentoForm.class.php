<?php
/**
 * MedicamentoForm Registration
 * @author  <your name here>
 */
class MedicamentoForm extends TPage
{
    protected $form; // form
    
    /**
     * Class constructor
     * Creates the page and the registration form
     */
    function __construct()
    {
        parent::__construct();
        
        // creates the form
        $this->form = new TForm('form_Medicamento');
        $this->form->class = 'tform'; // CSS class
        $this->form->style = 'width: 100%';
        
        // add a table inside form
        $table = new TTable;
        $table-> width = '100%';
        $this->form->add($table);
        
        // add a row for the form title
        $row = $table->addRow();
        $row->class = 'tformtitle'; // CSS class
        $row->addCell( new TLabel('Medicamento') )->colspan = 2;
        


        // create the form fields
        $id                             = new TEntry('id');
        $nome                           = new TEntry('nome');
        $dt_venc                        = new TDate('dt_venc');
        $dt_fab                         = new TDate('dt_fab');


        // define the sizes
        $id->setSize(500);
        $nome->setSize(500);
        $dt_venc->setSize(500);
        $dt_fab->setSize(500);

        // editable
        $id->setEditable( FALSE );
        
        // validations
        $nome->addValidation('nome', new TRequiredValidator);
        $dt_venc->addValidation('dt_venc', new TRequiredValidator);
        $dt_fab->addValidation('dt_fab', new TRequiredValidator);


        // add one row for each form field
        $table->addRowSet( new TLabel('ID'), $id );
        $table->addRowSet( $label_nome = new TLabel('Nome'), $nome );
        $label_nome->setFontColor('#FF0000');
        $table->addRowSet( $label_dt_venc = new TLabel('Data Venc.'), $dt_venc );
        $label_dt_venc->setFontColor('#FF0000');
        $table->addRowSet( $label_dt_fab = new TLabel('Data Fab.'), $dt_fab );
        $label_dt_fab->setFontColor('#FF0000');


        $this->form->setFields(array($id,$nome,$dt_venc,$dt_fab));


        // create the form actions
        $save_button = TButton::create('save', array($this, 'onSave'), _t('Save'), 'ico_save.png');
        $new_button  = TButton::create('new',  array($this, 'onEdit'), _t('New'),  'ico_new.png');
        
        $this->form->addField($save_button);
        $this->form->addField($new_button);
        
        $buttons_box = new THBox;
        $buttons_box->add($save_button);
        $buttons_box->add($new_button);
        
        // add a row for the form action
        $row = $table->addRow();
        $row->class = 'tformaction'; // CSS class
        $row->addCell($buttons_box)->colspan = 2;
        
        parent::add($this->form);
    }

    /**
     * method onSave()
     * Executed whenever the user clicks at the save button
     */
    function onSave()
    {
        try
        {
            TTransaction::open('db_consultas'); // open a transaction
            
            // get the form data into an active record Medicamento
            $object = $this->form->getData('Medicamento');
            $this->form->validate(); // form validation
            $object->store(); // stores the object
            $this->form->setData($object); // keep form data
            TTransaction::close(); // close the transaction
            
            // shows the success message
            new TMessage('info', TAdiantiCoreTranslator::translate('Record saved'));
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', '<b>Error</b> ' . $e->getMessage()); // shows the exception error message
            $this->form->setData( $this->form->getData() ); // keep form data
            TTransaction::rollback(); // undo all pending operations
        }
    }
    
    /**
     * method onEdit()
     * Executed whenever the user clicks at the edit button da datagrid
     */
    function onEdit($param)
    {
        try
        {
            if (isset($param['key']))
            {
                $key=$param['key'];  // get the parameter $key
                TTransaction::open('db_consultas'); // open a transaction
                $object = new Medicamento($key); // instantiates the Active Record
                $this->form->setData($object); // fill the form
                TTransaction::close(); // close the transaction
            }
            else
            {
                $this->form->clear();
            }
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', '<b>Error</b> ' . $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }
}

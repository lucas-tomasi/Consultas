<?php
/**
 * Atendimento Registration
 * @author  <your name here>
 */
class Atendimento extends TStandardForm
{
    protected $form; // form
    
    /**
     * Class constructor
     * Creates the page and the registration form
     */
    function __construct()
    {
        parent::__construct();
        
        parent::setDatabase('db_consultas');              // defines the database
        parent::setActiveRecord('Consulta');     // defines the active record
        
        // creates the form
        $this->form = new TQuickForm('form_Consulta');
        $this->form->class = 'tform'; // CSS class
        $this->form->style = 'width: 500px';
        
        // define the form title
        $this->form->setFormTitle('Consulta');
        


        // create the form fields
        $id                             = new TEntry('id');
        $dt_consulta                    = new TDate('dt_consulta');
        $turno                          = new TEntry('turno');
        $consultorio_id                 = new TEntry('consultorio_id');
        $estado_consulta_id             = new TEntry('estado_consulta_id');
        $system_user_id                 = new TEntry('system_user_id');
        $paciente_id                    = new TEntry('paciente_id');
        $diagnostico                    = new TText('diagnostico');


        // add the fields
        $this->form->addQuickField('ID', $id,  200);
        $this->form->addQuickField('Data Consulta', $dt_consulta,  200);
        $this->form->addQuickField('Turno', $turno,  200);
        $this->form->addQuickField('Consultorio', $consultorio_id,  100);
        $this->form->addQuickField('Estado Consulta', $estado_consulta_id,  200);
        $this->form->addQuickField('Medico', $system_user_id,  100);
        $this->form->addQuickField('Paciente', $paciente_id,  100);
        $this->form->addQuickField('diagnostico', $diagnostico,  200);


        $diagnostico->setSize(200, 40);


        
        // create the form actions
        $this->form->addQuickAction(_t('Save'), new TAction(array($this, 'onSave')), 'ico_save.png');
        $this->form->addQuickAction(_t('New'),  new TAction(array($this, 'onEdit')), 'ico_new.png');
        
        // add the form to the page
        parent::add($this->form);
    }
}

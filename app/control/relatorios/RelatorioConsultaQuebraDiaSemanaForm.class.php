<?php
/**
 * RelatorioConsultaForm Registration
 * @author  <your name here>
 */
class RelatorioConsultaQuebraDiaSemanaForm extends TStandardForm
{
    protected $form; // form
    
    /**
     * Class constructor
     * Creates the page and the registration form
     */
    function __construct()
    {
        parent::__construct();
        
        parent::setDatabase('db_consultas');              // defines the database
        parent::setActiveRecord('Consulta');     // defines the active record
        
        // creates the form
        $this->form = new TQuickForm('form_Consulta');
        $this->form->class = 'tform'; // CSS class
        $this->form->style = 'width: 100%';
        
        // define the form title
        $this->form->setFormTitle('Relatório Consulta Dia da Semana');
        


        // create the form fields
        $formato                        = new TRadioGroup('formato');

        
        
        $formato->addItems( array( "pdf" => '<i class="fa fa-file-pdf-o">  PDF</i>' , 
                                   'html' =>'<i class="fa fa-html5">  HTML</i>' , 
                                   'rtf' => '<i class="fa fa-file-word-o">  RTF</i>' , 
                                   'xls' => '<i class="fa fa-file-excel-o">  XLS</i>' ) );
        $formato->setLayout( "horizontal");
        
        $formato->setValue('pdf');
        
        // add the fields

        $this->form->addQuickField('Formato:', $formato,   1000);

        
        // create the form actions
        $this->form->addQuickAction( 'Enviar' , new TAction(array($this, 'onSend')), 'ico_down.png');
        
        // add the form to the page
        parent::add($this->form);
        
    }
    
    public function onSend()
    {
        $dados = $this->form->getData();
        try
        {
            // instantiate Soap Client
            $client = new SoapClient(NULL, array('encoding'   => 'UTF-8',
                                                  'exceptions' => TRUE,
                                                  'location'   => 'http://reports.univates.br/ws/report.ws.php',
                                                  'uri'        => "http://test-uri/",
                                                  'trace'      => 1 ) );
            // Portal user and password
            $login     = 'admin';
            $password  = 'rpt2011univates';
            $output    = 'app/output/ConsultasPorDiaDaSemana';
        
            $formato = $dados->formato;
            
            $report  = '/treino/Lucas/ConsultasPorDiaDaSemana.rpt';
            $param = array();

            $param['format'] = $formato;
            $param['style']  = 'bluescale';
            $content = $client-> generateReport($login, $password, $report, $param);
            $filename = $output.'.'.$formato;
            file_put_contents($filename,  base64_decode($content));
            
            new TMessage('info', "Relatório gerado com sucesso. Clique <a target='newwindow' href='{$filename}'>aqui</a> para visualizar");
            
            $dados = $this->form->setData( $dados );
        }
        catch (Exception $e)
        {
            new TMessage ( 'error' , $e->getMessage()."<br>\n" );
        }
    }
}

?>
<?php
/**
 * RelatorioConsultaForm Registration
 * @author  <your name here>
 */
class GraficoConsultasPorEstado extends TStandardForm
{
    protected $form; // form
    
    /**
     * Class constructor
     * Creates the page and the registration form
     */
    function __construct()
    {
        parent::__construct();
        
        parent::setDatabase('db_consultas');              // defines the database
        parent::setActiveRecord('Consulta');     // defines the active record
        
        // creates the form
        $this->form = new TQuickForm('form_Consulta');
        $this->form->class = 'tform'; // CSS class
        $this->form->style = 'width: 100%';
        
        // define the form title
        $this->form->setFormTitle('Gráfico Consultas Por Estado');
        


        // create the form fields        
        $dt_ini  = new TDate('dt_ini');
        $dt_fin  = new TDate('dt_fin'); 
      
        
        
        // add the fields

        $this->form->addQuickField('De' , $dt_ini , 500 , new TRequiredValidator );
        $this->form->addQuickField('Até', $dt_fin , 500);
        
        // create the form actions
        $this->form->addQuickAction( 'Enviar' , new TAction(array($this, 'onSend')), 'ico_down.png');
        
        // add the form to the page
        parent::add($this->form);
        
    }
    
    public function onSend()
    {
        
        try
        {
        $this->form->validate();
        $dados = $this->form->getData();
        
            // instantiate Soap Client
            $client = new SoapClient(NULL, array('encoding'   => 'UTF-8',
                                                  'exceptions' => TRUE,
                                                  'location'   => 'http://reports.univates.br/ws/report.ws.php',
                                                  'uri'        => "http://test-uri/",
                                                  'trace'      => 1 ) );
            // Portal user and password
            $login     = 'admin';
            $password  = 'rpt2011univates';
            $output    = 'app/output/ConsultasPorEstado';
        
            $report  = '/treino/Lucas/GraficoPizzaPorEstado.rpt';
            $param = array();
            $param['parameters']['$dt_ini'] = $dados->dt_ini;
            $param['parameters']['$dt_fin'] = $dados->dt_fin;
            $param['charttype']  = 'pie'; // bar, line, pie
            $param['chartengine']= 'p'; // p=png, j=javascript
            $param['width']  = 600; // chart
            $param['height'] = 400; // chart
            $filename = $output.'.png';
            $content = $client-> generateReport($login, $password, $report, $param);
            file_put_contents($filename,  base64_decode($content));
            
            new TMessage('info', "Relatório gerado com sucesso. Clique <a target='newwindow' href='{$filename}'>aqui</a> para visualizar");

            $this->form->setData( $dados );
        }
        catch (Exception $e)
        {
            new TMessage ( 'error' ,  $e->getMessage()."<br>\n" );
        }
    }
}

?>
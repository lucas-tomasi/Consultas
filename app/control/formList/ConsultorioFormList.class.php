<?php
/**
 * ConsultorioFormList Registration
 * @author  <your name here>
 */
class ConsultorioFormList extends TStandardFormList
{
    protected $form; // form
    protected $datagrid; // datagrid
    protected $pageNavigation;
    
    /**
     * Class constructor
     * Creates the page and the registration form
     */
    function __construct()
    {
        parent::__construct();
        
        parent::setDatabase('db_consultas');            // defines the database
        parent::setActiveRecord('Consultorio');   // defines the active record
        parent::setDefaultOrder('id', 'asc');         // defines the default order
        
        // creates the form
        $this->form = new TQuickForm('form_Consultorio');
        $this->form->class = 'tform'; // CSS class
        $this->form->setFormTitle('Consultório'); // define the form title
                    


        // create the form fields
        $id                             = new TEntry('id');
        $sala                           = new TEntry('sala');
        $predio                         = new TEntry('predio');


        // add the fields
        $this->form->addQuickField('ID', $id,  500);
        $this->form->addQuickField('Sala', $sala,  500, new TRequiredValidator );
        $this->form->addQuickField('Prédio', $predio,  500);

        // editable    
        $id->setEditable( FALSE );
            
        
         // mask
         $predio->setMask( "999" );
         $sala->setMask("999");   
        
        // create the form actions
        $this->form->addQuickAction(_t('Save'), new TAction(array($this, 'onSave')), 'ico_save.png');
        $this->form->addQuickAction(_t('New'),  new TAction(array($this, 'onEdit')), 'ico_new.png');
        
        // creates a DataGrid
        $this->datagrid = new TQuickGrid;
        $this->datagrid->setHeight(320);
        $this->datagrid->width = '100%';

        // creates the datagrid columns
        $id = $this->datagrid->addQuickColumn('ID', 'id', 'right', 10);
        $sala = $this->datagrid->addQuickColumn('Sala', 'sala', 'right', 100);
        $predio = $this->datagrid->addQuickColumn('Prédio', 'predio', 'right', 100);
            
        
        // create the datagrid actions
        $edit_action   = new TDataGridAction(array($this, 'onEdit'));
        $delete_action = new TDataGridAction(array($this, 'onDelete'));
        
        // add the actions to the datagrid
        $this->datagrid->addQuickAction(_t('Edit'), $edit_action, 'id', 'ico_edit.png');
        $this->datagrid->addQuickAction(_t('Delete'), $delete_action, 'id', 'ico_delete.png');
        
        // create the datagrid model
        $this->datagrid->createModel();
        
        // creates the page navigation
        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->setAction(new TAction(array($this, 'onReload')));
        $this->pageNavigation->setWidth($this->datagrid->getWidth());
        
        // create the page container
        //$container = TVBox::pack( $this->form, $this->datagrid, $this->pageNavigation);
        $container =  new TElement('div');
        $container->add($this->form);
        $container->add($this->datagrid);
        $container->add($this->pageNavigation);
        parent::add($container);
    }
}

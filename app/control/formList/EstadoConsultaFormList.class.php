<?php
/**
 * EstadoConsultaFormList Registration
 * @author  <your name here>
 */
class EstadoConsultaFormList extends TStandardFormList
{
    protected $form; // form
    protected $datagrid; // datagrid
    protected $pageNavigation;
    
    /**
     * Class constructor
     * Creates the page and the registration form
     */
    function __construct()
    {
        parent::__construct();
        
        parent::setDatabase('db_consultas');            // defines the database
        parent::setActiveRecord('EstadoConsulta');   // defines the active record
        parent::setDefaultOrder('id', 'asc');         // defines the default order
        
        // creates the form
        $this->form = new TQuickForm('form_EstadoConsulta');
        $this->form->class = 'tform'; // CSS class
        $this->form->setFormTitle('Estado Consulta'); // define the form title
        $this->form->style = 'width:100%';


        // create the form fields
        $id                             = new TEntry('id');
        $descricao                      = new TEntry('descricao');


        // add the fields
        $this->form->addQuickField('id', $id,  500);
        $this->form->addQuickField('Descrição', $descricao,  500, new TRequiredValidator );

        // set Editable
        $id->setEditable( FALSE );

        
        // create the form actions
        $this->form->addQuickAction(_t('Save'), new TAction(array($this, 'onSave')), 'ico_save.png');
        $this->form->addQuickAction(_t('New'),  new TAction(array($this, 'onEdit')), 'ico_new.png');
        
        // creates a DataGrid
        $this->datagrid = new TQuickGrid;
        $this->datagrid->setHeight(320);
        

        // creates the datagrid columns
        $id = $this->datagrid->addQuickColumn('id', 'id', 'left', 100);
        $descricao = $this->datagrid->addQuickColumn('Descrição', 'descricao', 'left', 200);

        
        // create the datagrid actions
        $edit_action   = new TDataGridAction(array($this, 'onEdit'));
        $delete_action = new TDataGridAction(array($this, 'onDelete'));
        
        // add the actions to the datagrid
        $this->datagrid->addQuickAction(_t('Edit'), $edit_action, 'id', 'ico_edit.png');
        $this->datagrid->addQuickAction(_t('Delete'), $delete_action, 'id', 'ico_delete.png');
        $this->datagrid->width = '100%';
        // create the datagrid model
        $this->datagrid->createModel();
        
        // creates the page navigation
        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->setAction(new TAction(array($this, 'onReload')));
        $this->pageNavigation->setWidth($this->datagrid->getWidth());
        
        // create the page container
        //$container = TVBox::pack( $this->form, $this->datagrid, $this->pageNavigation);
        $container =  new TElement('div');
        $container->add($this->form);
        $container->add($this->datagrid);
        $container->add($this->pageNavigation);
        parent::add($container);
    }
}

<?php
class Calendario extends TPage 
{

    private $html;    
    private $consultasCalendario;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->html = new THtmlRenderer('app/resources/calendario.html');      
  
        $this->html->enableSection( 'main' );
        
        $this->montarDadosCalendario();
        
        $this->html->enableSection( 'consultas' , $this->consultasCalendario , true );
        
        parent::add($this->html);
        
    }
    
    public function getConsultas()
    {
        TTransaction::open('db_consultas');
        
        $criterio = new TCriteria();
        $criterio->add( new TFilter( 'system_user_id' , '=' , TSession::getValue('userid') ) );
        $criterio->add( new TFilter( 'estado_consulta_id', '=' , 1 ) );
        $criterio->add( new TFilter( 'dt_consulta' , '>=' , date('Y-m-d') ) );        
                
        $consultas = new TRepository( 'Consulta' );
        $consultas = $consultas->load( $criterio );
                    
        TTransaction::close();       
        return $consultas;
    }
    
    public function montarDadosCalendario()
    {
        $this->consultasCalendario = array();
        try
        {
        
            TTransaction::open('db_consultas');
            
            $consultas = $this->getConsultas();
            
            $diaSemConsultas = true;
            
            for ( $i=0 ; $i<10 ; $i++ )
            {
                $diaSemConsultas = true;
                
                foreach ( $consultas as $consulta )
                {                 
                    if ($consulta->dt_consulta == date('Y-m-d', strtotime("+$i days") ) )
                    {                
                        $diaSemConsultas = false;
       
                        $paciente = new Paciente( $consulta->paciente_id );
                         
                        if( $consulta->turno == 'M' )
                        {
                            if ( array_key_exists( $consulta->dt_consulta , $this->consultasCalendario ) )
                            {
                                $this->consultasCalendario[ $consulta->dt_consulta ]['paciente_manha'] = $paciente->nome;
                            }
                            else
                            {
                                $this->consultasCalendario[ $consulta->dt_consulta ] = array( 'codigoConsultaManha' => $consulta->id , 'paciente_manha' => $paciente->nome , 'paciente_tarde' => '' , 'dt_consulta' => $consulta->dt_consulta );
                            }
                        }
                        else
                        {
                            if ( array_key_exists( $consulta->dt_consulta , $this->consultasCalendario ) )
                            {
                                $this->consultasCalendario[ $consulta->dt_consulta ]['paciente_tarde'] = $paciente->nome;
                            }
                            else
                            {
                                $this->consultasCalendario[ $consulta->dt_consulta ] = array( 'codigoConsultaTarde' => $consulta->id ,'paciente_tarde' => $paciente->nome , 'paciente_manha' => '' , 'dt_consulta' => $consulta->dt_consulta );
                            }
                        }
                    }                     
                }
                
                if ( $diaSemConsultas )
                {
                    $this->consultasCalendario[] = array( 'paciente_tarde' => '' , 'paciente_manha' => '' , 'dt_consulta' => date('Y-m-d', strtotime("+$i days") ));       
                }
            }
            
            TTransaction::close();
            
        }
        catch ( Exception $e )
        {
            new TMessage ( 'error' , $e->getMessage() );
            
            TTransaction::rollback();
        }
    }
}

?>
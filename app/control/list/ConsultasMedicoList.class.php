<?php
/**
 * ConsultasMedicoList Listing
 * @author  <your name here>
 */
class ConsultasMedicoList extends TStandardList
{
    protected $form;     // registration form
    protected $datagrid; // listing
    protected $pageNavigation;
    
    /**
     * Class constructor
     * Creates the page, the form and the listing
     */
    public function __construct($param)
    {
        parent::__construct();
        
        parent::setDatabase('db_consultas');            // defines the database
        parent::setActiveRecord('Consulta');   // defines the active record
        parent::setDefaultOrder('id', 'asc');         // defines the default order
        parent::addFilterField('id', '='); // add a filter field
        parent::addFilterField('estado_consulta_id', '='); // add a filter field
        
        // creates the form, with a table inside
        $this->form = new TQuickForm('form_search_Consulta');
        $this->form->class = 'tform'; // CSS class
        $this->form->setFormTitle('Consulta');
        $this->form->style = 'width:100%';

        // create the form fields
        $id                             = new TEntry('id');
        $estado_consulta_id             = new TDBCombo( 'estado_consulta_id' , 'db_consultas' , 'EstadoConsulta' , 'id' , 'descricao' );


        // add the fields
        $this->form->addQuickField('ID', $id,  500);
        $this->form->addQuickField('Estado Consulta', $estado_consulta_id,  500);
    
        // setValues
        $estado_consulta_id->setDefaultOption( false );       
        
        
        // keep the form filled during navigation with session data
        $this->form->setData( TSession::getValue('Consulta_filter_data') );
        
        // add the search form actions
        $this->form->addQuickAction(_t('Find'), new TAction(array($this, 'onSearch')), 'ico_find.png');
        $this->form->addQuickAction(_t('New'),  new TAction(array('ConsultaForm', 'onEdit')), 'ico_new.png');
        
        // creates a DataGrid
        $this->datagrid = new TQuickGrid;
        $this->datagrid->setHeight(320);
        $this->datagrid->width = '100%';
        
        // addFiltro pelo medico logado
        $criteria = new TCriteria();
        $criteria->add(new TFilter('system_user_id', '=', TSession::getValue('userid')));
        parent::setCriteria ($criteria );

        // creates the datagrid columns
        $id = $this->datagrid->addQuickColumn('ID', 'id', 'right', 50, new TAction(array($this, 'onReload')), array('order', 'id'));
        $dt_consulta = $this->datagrid->addQuickColumn('Data Consulta', 'dt_consulta', 'right', 100, new TAction(array($this, 'onReload')), array('order', 'dt_consulta'));
        $estado_consulta_id = $this->datagrid->addQuickColumn('Estado Consulta', 'estado_consulta_id', 'left', 200, new TAction(array($this, 'onReload')), array('order', 'estado_consulta_id'));
        $paciente_id = $this->datagrid->addQuickColumn('Paciente', 'paciente_id', 'left', 150, new TAction(array($this, 'onReload')), array('order', 'paciente_id'));
        $turno = $this->datagrid->addQuickColumn('Turno', 'turno', 'right', 50, new TAction(array($this, 'onReload')), array('order', 'turno'));

        // setTransformer
        $paciente_id->setTransformer( array( $this, 'onPaciente' ) ); 
        $estado_consulta_id->setTransformer( array( $this, 'onEstadoConsulta' ) ); 
        
        
        // create the datagrid actions
        $edit_action   = new TDataGridAction(array('ConsultaMedicoForm', 'onEdit'));
        
        // add the actions to the datagrid
        $this->datagrid->addQuickAction(_t('Edit'), $edit_action, 'id', 'ico_edit.png');
        
        // create the datagrid model
        $this->datagrid->createModel();
        
        // create the page navigation
        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->setAction(new TAction(array($this, 'onReload')));
        $this->pageNavigation->setWidth($this->datagrid->getWidth());
        
        // create the page container
        //$container = TVBox::pack( $this->form, $this->datagrid, $this->pageNavigation);
        $container =  new TElement('div');
        $container->add($this->form);
        $container->add($this->datagrid);
        $container->add($this->pageNavigation);
        parent::add($container);
        
        $this->onSetup();
    }
    
    public function onSetup()
    {
        if (!TSession::getValue($this->activeRecord.'_filter_data'))
        {
            $filter = new TFilter('estado_consulta_id', '=', 1);
            TSession::setValue($this->activeRecord.'_filter_estado_consulta_id', $filter);
            $this->onReload();
        }
    
    }
    
    public function onPaciente( $paciente_id )
    {
        $paciente = new Paciente( $paciente_id );

        return $paciente->nome;
    }
    public function onEstadoConsulta( $estado_consulta_id )
    {
        try
        {
            TTransaction::open('db_consultas');
            
                $estado = new EstadoConsulta( $estado_consulta_id );
                
            TTransaction::close();
        }
        catch (Exception $e )
        {
            new TMessage( 'error' , $e->getMessage() );
        }

        return $estado->descricao;
    }
}

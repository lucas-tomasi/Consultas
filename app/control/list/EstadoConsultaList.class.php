<?php
/**
 * EstadoConsultaList Listing
 * @author  <your name here>
 */
class EstadoConsultaList extends TStandardList
{
    protected $form;     // registration form
    protected $datagrid; // listing
    protected $pageNavigation;
    
    /**
     * Class constructor
     * Creates the page, the form and the listing
     */
    public function __construct()
    {
        parent::__construct();
        
        parent::setDatabase('db_consultas');            // defines the database
        parent::setActiveRecord('EstadoConsulta');   // defines the active record
        parent::setDefaultOrder('id', 'asc');         // defines the default order
        parent::addFilterField('id', '='); // add a filter field
        
        // creates the form, with a table inside
        $this->form = new TQuickForm('form_search_EstadoConsulta');
        $this->form->class = 'tform'; // CSS class
        $this->form->setFormTitle('EstadoConsulta');
        $this->form->style = 'width:100%';

        // create the form fields
        $id                             = new TEntry('id');


        // add the fields
        $this->form->addQuickField('id', $id,  100);



        
        // keep the form filled during navigation with session data
        $this->form->setData( TSession::getValue('EstadoConsulta_filter_data') );
        
        // add the search form actions
        $this->form->addQuickAction(_t('Find'), new TAction(array($this, 'onSearch')), 'ico_find.png');
        $this->form->addQuickAction(_t('New'),  new TAction(array('EstadoConsultaForm', 'onEdit')), 'ico_new.png');
        
        // creates a DataGrid
        $this->datagrid = new TQuickGrid;
        $this->datagrid->setHeight(320);
        $this->datagrid->width = '100%';

        // creates the datagrid columns
        $id = $this->datagrid->addQuickColumn('id', 'id', 'right', 100);
        $descricao = $this->datagrid->addQuickColumn('descricao', 'descricao', 'left', 200);

        
        // create the datagrid actions
        $edit_action   = new TDataGridAction(array('EstadoConsultaForm', 'onEdit'));
        $delete_action = new TDataGridAction(array($this, 'onDelete'));
        
        // add the actions to the datagrid
        $this->datagrid->addQuickAction(_t('Edit'), $edit_action, 'id', 'ico_edit.png');
        $this->datagrid->addQuickAction(_t('Delete'), $delete_action, 'id', 'ico_delete.png');
        
        // create the datagrid model
        $this->datagrid->createModel();
        
        // create the page navigation
        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->setAction(new TAction(array($this, 'onReload')));
        $this->pageNavigation->setWidth($this->datagrid->getWidth());
        
        // create the page container
        //$container = TVBox::pack( $this->form, $this->datagrid, $this->pageNavigation);
        $container =  new TElement('div');
        $container->add($this->form);
        $container->add($this->datagrid);
        $container->add($this->pageNavigation);
        parent::add($container);
    }
}

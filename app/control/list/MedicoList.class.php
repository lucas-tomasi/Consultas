<?php
/**
 * MedicoList Listing
 * @author  <your name here>
 */
class MedicoList extends TStandardList
{
    protected $form;     // registration form
    protected $datagrid; // listing
    protected $pageNavigation;
    
    /**
     * Class constructor
     * Creates the page, the form and the listing
     */
    public function __construct()
    {
        parent::__construct();
        
        parent::setDatabase('permission');            // defines the database
        parent::setActiveRecord('SystemUser');   // defines the active record
        parent::setDefaultOrder('id', 'asc');         // defines the default order
        parent::addFilterField('id', '='); // add a filter field
        parent::addFilterField('name', 'like'); // add a filter field
        
        // creates the form, with a table inside
        $this->form = new TQuickForm('form_search_SystemUser');
        $this->form->class = 'tform'; // CSS class
        $this->form->setFormTitle('Médicos');
        $this->form->style = 'width: 100%';

        // create the form fields
        $id                             = new TEntry('id');
        $name                           = new TEntry('name');


        // add the fields
        $this->form->addQuickField('ID', $id,  500);
        $this->form->addQuickField('Nome', $name,  500);

        

        
        // keep the form filled during navigation with session data
        $this->form->setData( TSession::getValue('SystemUser_filter_data') );
        
        // add the search form actions
        $this->form->addQuickAction(_t('Find'), new TAction(array($this, 'onSearch')), 'ico_find.png');
        $this->form->addQuickAction(_t('New'),  new TAction(array('MedicoForm', 'onEdit')), 'ico_new.png');
        
        // creates a DataGrid
        $this->datagrid = new TQuickGrid;
        $this->datagrid->setHeight(320);
        $this->datagrid->width = '100%';
            
        // filtro
        $criteria = new TCriteria();
        $criteria->add(new TFilter('id', 'in', 'NOESC:(SELECT system_user_id FROM system_user_group where system_group_id = 3)'));
        
        parent::setCriteria( $criteria );    
                
        // creates the datagrid columns
        $id = $this->datagrid->addQuickColumn('id', 'id', 'right', 50);
        $name = $this->datagrid->addQuickColumn('Nome', 'name', 'left', 250, new TAction(array($this, 'onReload')), array('order', 'name'));
        $email = $this->datagrid->addQuickColumn('E-mail', 'email', 'left', 250);

        
        // create the datagrid actions
        $edit_action   = new TDataGridAction(array('MedicoForm', 'onEdit'));
        $delete_action = new TDataGridAction(array($this, 'onDelete'));
        
        // add the actions to the datagrid
        $this->datagrid->addQuickAction(_t('Edit'), $edit_action, 'id', 'ico_edit.png');
        $this->datagrid->addQuickAction(_t('Delete'), $delete_action, 'id', 'ico_delete.png');      
                 
        // create the datagrid model
        $this->datagrid->createModel();
        // create the page navigation
        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->setAction(new TAction(array($this, 'onReload')));
        $this->pageNavigation->setWidth($this->datagrid->getWidth());
        
        // create the page container
        //$container = TVBox::pack( $this->form, $this->datagrid, $this->pageNavigation);
        $container =  new TElement('div');
        $container->add($this->form);
        $container->add($this->datagrid);
        $container->add($this->pageNavigation);
        parent::add($container);
    }     

}




























